UvedomitelGTDWin
================

Если вы не знаете, что такое "Альта-ГТД", то эта программа вам не нужна.
Если знаете - эта программа подключается к базе Альты, и следит за поступлением новых эд-2 сообщений по поданным
в таможню декларациям.
При получении новых сообщений программа отсылает информацию об с помощью XMPP на заданный в настройках JID (jabber-аккаунт)
Таким образом декларант узнаёт, что нужно бежать к компу отвечать на запросы :)

В программе используется библиотека agsXMPP (http://www.ag-software.net/agsxmpp-sdk/)
С исходным кодом можете делать всё, что захочется.