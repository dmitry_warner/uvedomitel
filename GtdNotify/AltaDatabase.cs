﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;

namespace GtdNotify
{
    struct EdMsg
    {
        public string ProccessID;
        public string MessageType;
        public string Result;
        public string InOutDateTime;
        public string DocumentName;
    }

    class AltaDatabase
    {

        public string server { get; set; } //ip адрес mssql сервера
        public string user { get; set; } //логин
        public string password { get; set; } // пароль
        public string database { get; set; } // имя базы данных на сервере
        public string firmName { get; set; } // Имя фирмы из конфига (будет отображаться на кнопках и в разных окошках, чтобы легче сориентироваться
        public bool check { get; set; } //нужно ли отслеживать данную базу данных
        public string name { get; private set; } // Имя секции ini файла. Используется для сохранения конфига.
        private SqlConnection sconn { get; set; } //
        public string whoToNotify { get; set; }

        //Два списка для id деклараций, поданных по эд.
        //Запрос не показывает декларации со статусом выпуск разрешен, соответственно когда приходит сообщение об этом, в базе статус меняется, потом программа лезет проверять новые сообщения, а эта декларация уже
        //не отражается в результатах запроса, поэтому получается невозможно выдать пользователю информацию о том, что декларация выпущена.
        //Поэтому я придумал сохранять результат запроса как список. И когда срабатывает таймер на следующий запрос его результат записывается во второй список.
        //потом они состыковываются. И выпущенная декларация исчезает из списка проверки не сразу а через один интервал проверки, что позволяет уведомить пользователя.
        private List<string> oldResult;
        private List<string> newResult;

        //Для отрисовки формы
        public Panel panel;
        public Label titleLabel;
        public Label infoLabel;

        public void ShowBox (int x, int y, int numBox)
        {
            panel = new Panel();
            titleLabel = new Label();
            infoLabel = new Label();
            int boxWidth = 190, boxHeight = 90;
            panel.Size = new Size(boxWidth, boxHeight);
            panel.Location = new Point(x, y);
            panel.BorderStyle = BorderStyle.FixedSingle;
            panel.Click += new EventHandler(panel_Click);

            titleLabel.AutoSize = true;
            titleLabel.Location = new Point(10, 10);
            titleLabel.Text = this.firmName;

            infoLabel.AutoSize = true;
            infoLabel.Location = new Point(10, 30);
            if (this.check)
            {
                infoLabel.Text = "Отслеживается деклараций: ";
            }
            else
            {
                infoLabel.Text = "Не отслеживается ";
            }
            
            if (!this.check) { panel.BackColor = Color.LightPink; } else { panel.BackColor = Color.LightGreen; }

        }
        private void panel_Click(object sender, EventArgs e)
        {
            Panel pan = (Panel)sender;
            if (this.check)
            {
                pan.BackColor = Color.LightPink;
                this.infoLabel.Text = "Не отслеживается";
                this.check = false;
            }
            else
            {
                pan.BackColor = Color.LightGreen;
                this.infoLabel.Text = "Отслеживается деклараций: ";
                this.check = true;
            }


        }

        /// <summary>
        /// Конструктор, разбирает секцию ини-файла с параметрами
        /// </summary>
        /// <param name="section">Секция ini-файла</param>
        public AltaDatabase(IniFile.IniSection section)
        {
            oldResult = new List<string>();
            newResult = new List<string>();

            name = section.Name;
            foreach (IniFile.IniSection.IniKey k in section.Keys)
            {
                switch (k.Name)
                {
                    case "server":
                        server = k.Value;
                        break;
                    case "username":
                        user = k.Value;
                        break;
                    case "password":
                        password = k.Value;
                        break;
                    case "database":
                        database = k.Value;
                        break;
                    case "firmName":
                        firmName = k.Value;
                        break;
                    case "check":
                        check = Convert.ToBoolean(k.Value);
                        break;
                    case "whoToNotify":
                        whoToNotify = k.Value;
                        break;
                    default:
                        System.Windows.Forms.MessageBox.Show("Нераспознанная строка в " + section.Name.ToString());
                        break;
                }
            }
            sconn = GetConnectionString();
        }
        /// <summary>
        /// Создаёт эк
        /// </summary>
        /// <param name="Server">Адрес сервера</param>
        /// <param name="User">Имя пользователя</param>
        /// <param name="Password">Пароль</param>
        /// <param name="Database">Имя базы данных</param>
        /// <param name="FirmName">Название фирмы(отображается в интерфейсе, для удобства, т.к. имя бд может быть незапоминающимся</param>
        /// <param name="Check">True если нужно отслеживать сообщения по этой, False - если нет </param>
        /// <param name="Notify"> Jabber ID, кому слать уведомления по этой фирме</param>
        public AltaDatabase(string Server, string User, string Password, string Database, string FirmName, bool Check, string Notify)
        {
            server = Server;
            user = User;
            password = Password;
            database = Database;
            firmName = FirmName;
            check = Check;
            name = "firm" + DateTime.Now.ToString();
            sconn = GetConnectionString();
            whoToNotify = Notify;
            oldResult = new List<string>();
            newResult = new List<string>();

        }
        public AltaDatabase()
        {
            server = "localhost";
            user = "sa";
            password = "";
            database = "database1";
            firmName = "Рога и копыта";
            check = false;
            name = "firm" + DateTime.Now.ToString();
            sconn = GetConnectionString();
            whoToNotify = "example@example.com";
        }
        public bool TestConnect()
        {
            try
            {
                this.GetConnectionString().Open();
                this.GetConnectionString().Close();
            }
            catch (Exception ex)
            {               
                this.check = false;
                return false;
            }
            return true;
        }

        private SqlConnection GetConnectionString()
        {
            SqlConnection connection = new SqlConnection("Data Source=" + this.server + ";Initial Catalog=" + this.database + ";User Id=" + this.user + ";Password=" + this.password + ";");
            return connection;
        }
        
        public List<string> GetGtdList()
        {
            this.sconn.Open();
            SqlCommand cmd = this.sconn.CreateCommand();
            //запрос выбирает идентификаторы все гтд которые не в корзине (pathgid!=...) и у которых статусы не следующие - 7(Выпуск разрешён),
            // статус 0 - без эд-статуса, статус 6 - условно выпущена, статус 10 отказано в выпуске, статус 12 - выпуск с обеспечением, статус 13 - тд отозвана, 8192 - отказ в регистрации, 10240 - отказ в регистрации (приеме), 32768 - отозвана
            //статус 14 считается не поданной
            cmd.CommandText = "SELECT GID FROM [Docs] WHERE DOCTYP = 'GTD' AND EDSTAT!=7 AND EDSTAT!=0 AND EDSTAT!=6 AND EDSTAT!=10 AND EDSTAT!=12 AND EDSTAT!=13 AND EDSTAT!=14 AND EDSTAT!=2055 AND EDSTAT!=8192 AND EDSTAT!=32768 AND EDSTAT!=10240 AND PATHGID!='00000000-0000-0000-0000-000000000002'";
            SqlDataReader dr = cmd.ExecuteReader();
           
            
            

            
            if (dr.HasRows)
            {
                while (dr.Read())
                {                    
                    newResult.Add(dr["GID"].ToString());
                }
            }
            else
            {                
                newResult.Add("");
            }
            //Сохраняем результаты во временном списке
            List<string> tempList = new List<string>(newResult);
            
            if (oldResult.Count != 0)
            {
                //Собираем два списка вместе            
                newResult.AddRange(oldResult);
            }

            //убираем дубликаты
            List<string> noDupes = newResult.Distinct().ToList();                   
            
            //копируем в oldList результат текущего запроса
            oldResult.Clear();
            oldResult.AddRange(tempList);

            newResult.Clear();
            
            this.sconn.Close();
            return noDupes;
        }

       
        public string[] GetGtdInfo(string GID)
        {
            this.sconn.Open();
            SqlCommand cmd = this.sconn.CreateCommand();
            cmd.CommandText = "SELECT GTDList.[2], GTDList.[8], GTDList.[18], GTDList.[BLOCK\\1\\31], Docs.REGNUM, Docs.DOCKEY FROM GTDList INNER JOIN Docs ON GTDList.DOCGID = Docs.GID WHERE GTDList.DOCGID = '" + GID + "'";
            SqlDataReader dr = cmd.ExecuteReader();
            string[] info = new string[6];
            if (!dr.HasRows) { System.Windows.Forms.MessageBox.Show("В БД" + database + "не найдена информация о декларации с идентификатором " + GID); }
            else
            {
                dr.Read();
                info[0] = dr["2"].ToString();//отправитель
                info[1] = dr["8"].ToString();//получатель
                info[2] = dr["18"].ToString();//Номера машин
                info[3] = dr["BLOCK\\1\\31"].ToString();//описание первого товара
                info[4] = dr["REGNUM"].ToString();//таможенный номер декларации
                info[5] = dr["DOCKEY"].ToString();//внутренний номер декларации
            }
            this.sconn.Close();
            return info;

        }

        public List<EdMsg> GetEdMessages(string GID, string MessageFilter)
        {
            this.sconn.Open();
            SqlCommand cmd = this.sconn.CreateCommand();
            /*
             * TODO - Добавить сюда в запрос ограничения по типам сообщений. И также можно фильтровать по дате, чтобы сервер возвращал только те, у которых дата больше, чем время последней проверки
             * Хотя, наверное стоит для этого сделать отдельный метод GetNewEdMessages
             * Ограничить в запросе время создания сообщений я не смог, что-то MSSQL не поддался. А потом забил.
             * */
            cmd.CommandText = "Select ProccessID, MessageType, InOutDateTime, DocumentName, Result  from EDMsgs  Where DocGuid = '" + GID + "' AND Incoming = 1 " + MessageFilter + " ORDER BY InOutDateTime DESC";
            SqlDataReader dr = cmd.ExecuteReader();
            List<EdMsg> messages = new List<EdMsg>();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    EdMsg msg = new EdMsg();
                    msg.ProccessID = dr["ProccessID"].ToString();
                    msg.DocumentName = dr["DocumentName"].ToString();                    
                    msg.InOutDateTime = dr["InOutDateTime"].ToString();
                    msg.MessageType = dr["MessageType"].ToString();
                    msg.Result = dr["Result"].ToString();
                    messages.Add(msg);
                }
            }
            else
            {
                EdMsg msg = new EdMsg();
                msg.ProccessID = "";
                msg.DocumentName = "";
                msg.InOutDateTime = "";
                msg.MessageType = "";
                msg.Result = "";
                messages.Add(msg);
            }
            this.sconn.Close();
            return messages;
        }
                

        public override string ToString()
        {                
            return this.firmName;
        }

        public IniFile SaveSettings()
        {
            IniFile settings = new IniFile();
            settings.AddSection(this.name).AddKey("server").Value = this.server;
            settings.AddSection(this.name).AddKey("username").Value = this.user;
            settings.AddSection(this.name).AddKey("password").Value = this.password;
            settings.AddSection(this.name).AddKey("database").Value = this.database;
            settings.AddSection(this.name).AddKey("firmName").Value = this.firmName;
            settings.AddSection(this.name).AddKey("check").Value = Convert.ToString(this.check);
            settings.AddSection(this.name).AddKey("whoToNotify").Value = this.whoToNotify;
            return settings;

        }

    }
}
