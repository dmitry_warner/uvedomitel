﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace GtdNotify
{
    

    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }
        

        string configPath = Application.StartupPath + "\\config.conf";

      

        private void button1_Click(object sender, EventArgs e)
        {
            AltaDatabase adb = new AltaDatabase(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text, checkBox1.Checked, jabberId.Text);
            listBox1.Items.Add(adb);            
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count == 1)
            {
                AltaDatabase warner = new AltaDatabase();
                warner = listBox1.SelectedItem as AltaDatabase;

                textBox1.Text = warner.server;
                textBox2.Text = warner.user;
                textBox3.Text = warner.password;
                textBox4.Text = warner.database;
                textBox5.Text = warner.firmName;
                checkBox1.Checked = warner.check;
                jabberId.Text = warner.whoToNotify;
                button1.Enabled = false;
                button3.Enabled = true;
                button3.Visible = true;
                button2.Enabled = true;
            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AltaDatabase warner = new AltaDatabase();
            warner = listBox1.SelectedItem as AltaDatabase;
            warner.server = textBox1.Text;
            warner.user = textBox2.Text;
            warner.password = textBox3.Text;
            warner.database = textBox4.Text;
            warner.firmName = textBox5.Text;
            warner.check = checkBox1.Checked;
            warner.whoToNotify = jabberId.Text;

            
            //Убираем текст из всех текстбоксов и отключаем чекбокс
                foreach (Control c in this.groupBox1.Controls)
                {
                    if (c is TextBox) { c.Text = ""; }
                    if (c is CheckBox) { CheckBox ch = c as CheckBox; ch.Checked = false; }
                }
           //Получаем индекс выделенного элемента (одновременно выделенным может быть только один)
            int elementIndex = listBox1.SelectedIndices[0];
           //Удаляем элемент из листбокса и вставляем изменённый на то-же место, чтобы вновь вызвалось свойство ToString() и имя обновилось.
            listBox1.Items.RemoveAt(elementIndex);
            listBox1.Items.Insert(elementIndex, warner);            
            button3.Enabled = false;
            button1.Enabled = true;
            
        }

        private void LoadConfig()
        {
            if (!File.Exists(configPath))
            {
                //Не обнаружен файл с конфигом. Создадим стандартный.
                IniFile temp = new IniFile();
                temp.AddSection("MessageDescriptions").AddKey("CMN.11034").Value = "Разрешение на отзыв (ЭК)";
                temp.AddSection("MessageDescriptions").AddKey("ED.11005").Value = "Перечень ошибок таможенного АРМ";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11033").Value = "Решение по ДТ (ЭК)";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11043").Value = "Условный выпуск(ИМ)";
                temp.AddSection("MessageDescriptions").AddKey("ED.11003").Value = "Запрошен документ";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11009").Value = "Регистрационный номер ДТ, дата и время завершения проверки";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11005").Value = "Уведомление о досмотре";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11031").Value = "Запрос инспектора на внесение изменений в ДТ.";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11007").Value = "Результаты таможенного контроля";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11012").Value = "Разрешение на отзыв ТД (ИМ)";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11015").Value = "Уведомление о поступлении товаров";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11010").Value = "Решение по декларации (ИМ)";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11002").Value = "Начата проверка";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11001").Value = "Присвоен номер";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11065").Value = "Уведомление о проведении дополнительной проверки таможенной стоимости";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11066").Value = "Уведомление о проведении дополнительной проверки по классификации товара";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11067").Value = "Уведомление о проведении дополнительной проверки по стране происхождении товара";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11064").Value = "Решение о корректировке таможенной стоимости";
                temp.AddSection("MessageDescriptions").AddKey("CMN.10003").Value = "Отказ в запрашиваемых действиях Декларанта";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11014").Value = "Разрешение на изменение, дополнение сведений, заявленных в таможенной декларации";
                temp.AddSection("MessageDescriptions").AddKey("CMN.11042").Value = "Выпуск товаров под обеспечение (ЭК)";
                
                temp.AddSection("NotificationSettings").AddKey("MQTTServer").Value = "localhost";
                temp.AddSection("NotificationSettings").AddKey("MQTTPort").Value = "1883";
                temp.AddSection("NotificationSettings").AddKey("MQTTID").Value = "Gtd-Inform";
                temp.AddSection("NotificationSettings").AddKey("DeviceIDs").Value = "dek-1;dek2";

                temp.AddSection("CheckThisType").AddKey("check").Value = "CMN.11001";

                try
                {
                    temp.Save(configPath);
                    LoadConfig();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                //Загрузка настроек в форму

                IniFile configFile = new IniFile();
                configFile.Load(configPath);
                foreach (IniFile.IniSection configSection in configFile.Sections)
                {
                    if (configSection.Name.Contains("firm"))
                    {
                        AltaDatabase firm = new AltaDatabase(configSection);
                        listBox1.Items.Add(firm);
                    }
                }

                //Загружаем настройки джаббер сервера
                tbJabberLogin.Text = configFile.GetKeyValue("JabberSettings", "Login");
                tbJabberPort.Text = configFile.GetKeyValue("JabberSettings", "Port");
                tbJabberPassword.Text = configFile.GetKeyValue("JabberSettings", "Password");
                
                

                //Загружаем строку для отслеживания
                //Получаем строку из секции CheckThisType с именем ключа check и разбиваем её на массив (разбивается по ;)
                string[] MessageCheckArray = configFile.GetKeyValue("CheckThisType", "check").Split(';');
                // Объявляем секцию с описанием сообщений и грузим её в ListView
                IniFile.IniSection MessageDescriptions = configFile.GetSection("MessageDescriptions");
                foreach (IniFile.IniSection.IniKey key in MessageDescriptions.Keys)
                {
                    ListViewItem lvi = new ListViewItem(key.Name);
                    lvi.SubItems.Add(key.Value);
                    if (MessageCheckArray.Contains(key.Name)) { lvi.Checked = true; }
                    listView1.Items.Add(lvi);

                }
            }
        }

        private void Settings_Load(object sender, EventArgs e)
        {

            LoadConfig();

        }
        
        private void Settings_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (MessageBox.Show("Сохранить изменения", "Настройки", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {                          

               
            }
            else
            {
                List<IniFile.IniSection> SettingSections = new List<IniFile.IniSection>();
                IniFile warnSettings = new IniFile();
                //Сохраним описания типов сообщений
                foreach (ListViewItem item in listView1.Items)
                {
                    warnSettings.AddSection("MessageDescriptions").AddKey(item.SubItems[0].Text).Value = item.SubItems[1].Text;                    
                }
                //Сохраним выделенные сообщения
                string checkMsg = "";
                foreach (ListViewItem item in listView1.CheckedItems)
                {
                    checkMsg += item.SubItems[0].Text + ";";
                }
                if (checkMsg == "")
                {
                    //если не выбрано ни одного типа сообщений для отслеживания
                    checkMsg = ";";
                }
                warnSettings.AddSection("CheckThisType").AddKey("check").Value = checkMsg;

                //Сохраним настройки уведомлений
                warnSettings.AddSection("JabberSettings").AddKey("Login").Value = tbJabberLogin.Text;
                warnSettings.AddSection("JabberSettings").AddKey("Port").Value = tbJabberPort.Text;
                warnSettings.AddSection("JabberSettings").AddKey("Password").Value = tbJabberPassword.Text;               
               

                //Сохраним данные по фирмам
                int i=0;
                foreach (AltaDatabase warnbase in listBox1.Items)
                {
                    IniFile OneFirm = warnbase.SaveSettings();
                    OneFirm.Save(Application.StartupPath + i + ".conf");
                    warnSettings.Load(Application.StartupPath + i + ".conf", true);
                    File.Delete(Application.StartupPath + i + ".conf");
                    i++;
                }

                try
                {
                    warnSettings.Save(configPath);
                   
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count == 1)
            {
                listBox1.Items.Remove(listBox1.SelectedItem);
                button3.Enabled = false;               
                button1.Enabled = true;
                button2.Enabled = false;
            }
        }
    }
}
