﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using agsXMPP;
using agsXMPP.protocol.client;
using System.Timers;
using System.Xml.Linq;
using System.Collections.Specialized;


namespace GtdNotify
{
    class Jabber
    {
        private XmppClientConnection XmppConn = new XmppClientConnection();

        public event EventHandler onLogin;
        public event EventHandler onClose;
        public event EventHandler onAuthError;

        public delegate void ChangeStatusHandler(Jabber jabb, string NewStatus);
        public event ChangeStatusHandler StatusChanged;

        public delegate void XmlRead(string xml);
        public event XmlRead xmlReceive;
        public event XmlRead xmlSend;

        private System.Timers.Timer delayBeforeReconnect = new System.Timers.Timer();
        private System.Timers.Timer deliveryReportCheckTimer = new System.Timers.Timer();

        //Для хранения сообщений
        struct JabberMessage
        {
            public string To;
            public string Message;
        }

        //База исходящих сообщений. Сообщения удаляются отсюда при получении отчёта о доставке
        Dictionary<string, JabberMessage> UndeliveredMessages = new Dictionary<string, JabberMessage>();



        public Jabber()
        {
            Jid jid = new Jid("test@localhost");
            XmppConn.Server = jid.Server;
            XmppConn.Username = jid.User;
            XmppConn.Password = "testtest";
            XmppConn.Resource = "Uvedomitel";
            XmppConn.Port = 5222;
            XmppConn.UseSSL = false;
            XmppConn.AutoResolveConnectServer = true;
            XmppConn.KeepAlive = true;
            Activate_Events();

        }
        /// <summary>
        ///    Создаёт подключение к серверу
        /// </summary>
        /// <param name="username">JID, напимер vasya@jabber.net</param>
        /// <param name="pasClsword">Пароль</param>
        /// <param name="port">Порт сервера, по умолчанию 5222</param>
        public Jabber(string username, string password, int port = 5222)
        {
            Jid jid = new Jid(username);
            XmppConn.Server = jid.Server;
            XmppConn.Username = jid.User;
            XmppConn.Password = password;
            XmppConn.Resource = "Uvedomitel";
            XmppConn.Port = port;
            XmppConn.UseSSL = false;
            XmppConn.AutoResolveConnectServer = true;
            XmppConn.KeepAlive = true;
            Activate_Events();
        }

        /// <summary>
        /// Выставляет обработчики евентов, которые генерируем xmpp библиотета. и заодно два таймера - один для переподключения в случае обрыва соединения. Другой - периодически дёргает проверку - на все ли отправленные сообщения пришли уведомления о прочтении?.
        /// </summary>
        private void Activate_Events()
        {
            delayBeforeReconnect.Interval = 30000; //30 секунд интервал до повторной попытки подключения
            delayBeforeReconnect.Elapsed += delayBeforeReconnect_Elapsed;

            deliveryReportCheckTimer.Interval = 600000; //(10 минут)
            deliveryReportCheckTimer.Elapsed += deliveryReportCheckTimer_Elapsed;

            XmppConn.SocketConnectionType = agsXMPP.net.SocketConnectionType.Direct;

            XmppConn.OnLogin += XmppConn_OnLogin;
            XmppConn.OnClose += XmppConn_OnClose;
            XmppConn.OnSocketError += XmppConn_OnSocketError;
            XmppConn.OnAuthError += XmppConn_OnAuthError;
            XmppConn.ClientSocket.OnValidateCertificate += ClientSocket_OnValidateCertificate;
            XmppConn.OnXmppConnectionStateChanged += XmppConn_OnXmppConnectionStateChanged;
            XmppConn.OnReadXml += XmppConn_OnReadXml;
            XmppConn.OnWriteXml += XmppConn_OnWriteXml;
            XmppConn.OnMessage += XmppConn_OnMessage;
        }


        void deliveryReportCheckTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ResendUndeliveredMessage();
        }


        void XmppConn_OnMessage(object sender, Message msg)
        {

            //Проверяем, если это входящее сообщение о прочтении, то делаем
            if (msg.InnerXml.ToString().Contains("received xmlns=\"urn:xmpp:receipts\""))
            {
                //Если сообщение о прочтении, то следующий код вытаскивает id сообщения
                string message_id_read = msg.FirstChild.Attributes["id"].ToString();
                //Удаляем из базы недоставленных
                UndeliveredMessages.Remove(message_id_read);
            }
            else
            {
                // Остальные сообщения игнорируем
            }
        }

        void XmppConn_OnWriteXml(object sender, string xml)
        {
            if (xmlSend != null) { xmlSend(xml); }
        }

        void XmppConn_OnReadXml(object sender, string xml)
        {
            if (xmlReceive != null) { xmlReceive(xml); }
        }

        void delayBeforeReconnect_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Сработал таймер переподключения
            delayBeforeReconnect.Stop();
            Connect();
        }

        private void XmppConn_OnSocketError(object sender, Exception ex)
        {

            this.Close();
            delayBeforeReconnect.Start();
        }

        private void XmppConn_OnClose(object sender)
        {
            if (onClose != null)
            {
                onClose(this, new EventArgs());
            }
        }

        private void XmppConn_OnXmppConnectionStateChanged(object sender, XmppConnectionState state)
        {

            if (StatusChanged != null) // если null значит на это событие никто не подписан
            {
                this.StatusChanged(this, state.ToString());

            }
            if (state == XmppConnectionState.Disconnected)
            {
                //Если отключились от джаббера, запускаем таймер на переподключение
                this.delayBeforeReconnect.Start();
            }

        }

        private bool ClientSocket_OnValidateCertificate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true; //Все сертификаты будем считать валидными
        }

        private void XmppConn_OnAuthError(object sender, agsXMPP.Xml.Dom.Element e)
        {
            if (onAuthError != null)
            {
                onAuthError(this, new EventArgs());
            }
        }

        private void XmppConn_OnLogin(object sender)
        {
            if (onLogin != null)
            {
                onLogin(this, new EventArgs());

            }

            //Запустим таймер периодической проверки недоставленности сообщений.
            if (!deliveryReportCheckTimer.Enabled)
            {
                deliveryReportCheckTimer.Start();
            }
            ResendUndeliveredMessage();
        }

        private void ResendUndeliveredMessage()
        {
            if (UndeliveredMessages.Count >= 1)
            {
                Dictionary<string, JabberMessage> tempDic = new Dictionary<string, JabberMessage>(UndeliveredMessages); // копируем словарь UndeliverdMessage в новый.
                foreach (KeyValuePair<string, JabberMessage> entry in tempDic)
                {
                    SendMessage(entry.Value.To, entry.Value.Message);
                    UndeliveredMessages.Remove(entry.Key);
                }
                tempDic.Clear();
            }
        }

        public void Connect()
        {
            this.XmppConn.Open();
        }

        public void SendMessage(string to, string message)
        {
            agsXMPP.protocol.client.Message msg = new agsXMPP.protocol.client.Message();

            msg.Type = MessageType.chat;
            msg.To = new Jid(to);
            msg.Body = message;
            msg.GenerateId();
            msg.AddChild(new agsXMPP.protocol.extensions.msgreceipts.Request()); //добавляем запрос на подтверждение прочтения (для этого и нужен id сообщения)
            //Добавляем сообщение в базу недоставленных
            JabberMessage toBase = new JabberMessage();
            toBase.To = to;
            toBase.Message = message;
            UndeliveredMessages.Add(msg.Id, toBase);
            //и теперь отсылаем
            this.XmppConn.Send(msg);
        }

        public void Close()
        {
            //Закрывает подключение и отключает таймер, чтобы переподключения не получалось.
            this.XmppConn.Close();
            delayBeforeReconnect.Stop();
            deliveryReportCheckTimer.Stop();
        }
    }
}
