﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MqttLib;
using agsXMPP;
using agsXMPP.protocol.client;

namespace GtdNotify
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();           
          
        }

        //Путь к файлу с конфигом
        string configPath = Application.StartupPath + "\\config.conf";       
        List<AltaDatabase> baseList = new List<AltaDatabase>();
        string[] MessageCheckArray;
        
        //Описания эд2 сообщений
        Dictionary<string,string> MessageDescriptions = new Dictionary<string,string>();
        string addtosql = ""; // Строка для подстановки в sql запрос (фильтр по типам эд-2 сообщений)

        private Jabber jabber_connection;
        
        //Процедура инициализации программы.
        private void LoadSettings()
        {
            // Загрузим инфу из конфига
            IniFile config = new IniFile();
            if (!File.Exists(configPath))
            {
                MessageBox.Show("Не найден конфигурационный файл по адресу\r\n" + configPath + " Будет открыто окно настроек.\r\n Потом перезапустите программу.", "Не найдены настройки", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Нужно открыть новую форму
                Settings SettForm = new Settings();
                
                SettForm.ShowDialog(this);
                this.Dispose();

            }
            else
            {
                this.Text = "ГТД Уведомитель. " + configPath.ToString();
                config.Load(configPath);
                //Заполняем словарь типов и описаний эд2 сообщений
                IniFile.IniSection MessageDesc = config.GetSection("MessageDescriptions");
                foreach (IniFile.IniSection.IniKey key in MessageDesc.Keys)
                {
                    MessageDescriptions.Add(key.Name, key.Value);
                }

                //Получаем строку, фильтрующую сообщения по типам
                if (config.GetKeyValue("CheckThisType", "check") == ";")
                {
                    //если не выбрано ни одного типа сообщений для отслеживания
                    MessageBox.Show("В настройках не выбрано ни одного типа ЭД-2 сообщений для отслеживания.\r\nПрограмма не будет выполнять свою работу","Проверьте настройки",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                }
                MessageCheckArray = config.GetKeyValue("CheckThisType", "check").Split(';');


                //Загрузка jabber настроек    
                string username = config.GetKeyValue("JabberSettings", "Login");
                string password = config.GetKeyValue("JabberSettings", "Password");
                int port = Convert.ToInt32(config.GetKeyValue("JabberSettings", "Port"));                
                jabber_connection = new Jabber(username, password, port);
                //будем обрабатывать некоторые евенты джаббера
                jabber_connection.StatusChanged += jabber_connection_StatusChanged;                
                



                //Загружаем инфу по фирмам

                int x = 10;
                int y = 10;
                int boxWidth = 190, boxHeight = 90;
                int countPanelInStr = 0;
                int i = 0;



                foreach (IniFile.IniSection configSection in config.Sections)
                {
                    if (configSection.Name.Contains("firm"))
                    {
                        //все фирмы в конфиге в названии раздела имеют слово firm
                        AltaDatabase firm = new AltaDatabase(configSection);
                        baseList.Add(firm);

                        firm.ShowBox(x, y, i);
                        Controls.Add(firm.panel);
                        firm.panel.Controls.Add(firm.infoLabel);
                        firm.panel.Controls.Add(firm.titleLabel);


                        x += boxWidth + 5;
                        countPanelInStr++;
                        if (countPanelInStr == 5)
                        {
                            countPanelInStr = 0;
                            y += boxHeight + 5;
                            x = 10;

                        }
                        i++;

                    }

                }

                //Вычисляем суммарный размер, занятый панелями с инфой о фирмах.
                //по 5 панелек в ряду
                //расстояние между панелями - 5px
                int totalX = 0;
                int totalY = 0;
                int rows = 0;
                rows = baseList.Count() / 5; //т.к. это int то результат будет целый
                if ((baseList.Count() % 5) > 0)
                {
                    //Если есть остаток от деления, значит есть ещё один неполный ряд панелек
                    rows++;
                }
                totalX = 10 + (5 + boxWidth) * 5;
                totalY = 10 + (5 + boxHeight) * rows;



                // А теперь, с учётом этих координат, нужно дорисовать остальные элементы формы.
                GroupBox gbx = new GroupBox();
                gbx.Name = "gbx_groupbox";
                gbx.Size = new Size(totalX - 15, 260);
                gbx.Location = new Point(10, totalY);
                gbx.Text = "Настройки проверки";
                Controls.Add(gbx);

                Label gbxLabel = new Label();
                gbxLabel.AutoSize = true;
                gbxLabel.Location = new Point(10, 20);
                gbxLabel.Text = "Проверять каждые: ";
                gbx.Controls.Add(gbxLabel);

                NumericUpDown gbxNumeric = new NumericUpDown();
                gbxNumeric.Name = "gbxNumeric";
                gbxNumeric.Location = new Point(124, 18);
                gbxNumeric.Value = 1;
                gbxNumeric.Increment = 1;
                gbxNumeric.Maximum = 100;
                gbxNumeric.Size = new Size(36, 20);
                gbx.Controls.Add(gbxNumeric);

                Label gbxLabel2 = new Label();
                gbxLabel2.AutoSize = true;
                gbxLabel2.Location = new Point(166, 20);
                gbxLabel2.Text = "минут ";
                gbx.Controls.Add(gbxLabel2);

                Button gbxStartStop = new Button();
                gbxStartStop.Text = "Старт";
                gbxStartStop.Name = "gbxStartStop";
                gbxStartStop.Size = new Size(100, 30);
                gbxStartStop.Location = new Point((gbx.Width / 2) - (gbxStartStop.Width / 2), 15);
                gbxStartStop.Click += gbxStartStop_Click;
                gbx.Controls.Add(gbxStartStop);

                Button gbxSettings = new Button();
                gbxSettings.Text = "Настройки программы";
                gbxSettings.Size = new Size(145, 30);
                Point loc = gbxStartStop.Location;
                gbxSettings.Location = new Point((gbx.Size.Width - gbxSettings.Size.Width - 5), 15);
                //loc.X+=5 + gbxStartStop.Width;
                //gbxSettings.Location = loc;
                gbxSettings.Click += gbxSettings_Click;
                gbx.Controls.Add(gbxSettings);


                // listView1
                // 
                ListView gbxListView = new ListView();

                ColumnHeader gbxListViewcolumnHeader1 = new ColumnHeader();
                ColumnHeader gbxListViewcolumnHeader2 = new ColumnHeader();
                ColumnHeader gbxListViewcolumnHeader3 = new ColumnHeader();
                ColumnHeader gbxListViewcolumnHeader4 = new ColumnHeader();
                ColumnHeader gbxListViewcolumnHeader5 = new ColumnHeader();


                gbxListView.AllowColumnReorder = true;
                gbxListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {                   
                gbxListViewcolumnHeader1,
                gbxListViewcolumnHeader2,
                gbxListViewcolumnHeader3,
                gbxListViewcolumnHeader4,
                gbxListViewcolumnHeader5});
                gbxListView.FullRowSelect = true;
                gbxListView.Location = new System.Drawing.Point(6, 50);
                gbxListView.Name = "gbxlistView";
                gbxListView.Size = new System.Drawing.Size(958, 199);
                gbxListView.Sorting = System.Windows.Forms.SortOrder.Descending;
                gbxListView.TabIndex = 9;
                gbxListView.UseCompatibleStateImageBehavior = false;
                gbxListView.View = System.Windows.Forms.View.Details;
                // 
                // columnHeader1
                // 
                gbxListViewcolumnHeader1.Text = "Время";
                gbxListViewcolumnHeader1.Width = 107;
                // 
                // columnHeader2
                // 
                gbxListViewcolumnHeader2.Text = "Фирма";
                gbxListViewcolumnHeader2.Width = 135;
                // 
                // columnHeader3
                // 
                gbxListViewcolumnHeader3.Text = "Тип";
                gbxListViewcolumnHeader3.Width = 77;
                // 
                // columnHeader4
                // 
                gbxListViewcolumnHeader4.Text = "Сообщение";
                gbxListViewcolumnHeader4.Width = 375;
                // 
                // columnHeader5
                // 
                gbxListViewcolumnHeader5.Text = "ГТД №";
                gbxListViewcolumnHeader5.Width = 258;

                gbx.Controls.Add(gbxListView);

                //Общий размер формы, сколько прибавить можно высчитать, я подобрал опытным путём
                totalY += 330;
                totalX += 25;
                this.Width = totalX;
                this.Height = totalY;

                //обработаем строку из конфига с типами интересующих нас эд-2 сообщений
                foreach (string s in MessageCheckArray)
                {
                    if (s != "")
                    {
                        if (addtosql == "")
                        {
                            addtosql = "AND (MessageType ='" + s + "' ";
                        }
                        else
                        {
                            addtosql += "OR MessageType ='" + s + "' ";
                        }
                    }


                }
                addtosql += ")"; // Добавим закрывающую скобку
            }
        }

        void jabber_connection_StatusChanged(Jabber jabb, string NewStatus)
        {
            toolStripStatusLabel_jabber.Text = NewStatus;
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void gbxSettings_Click(object sender, EventArgs e)
        {
            //Нужно открыть новую форму
            Settings SettForm = new Settings();
            this.Hide();
            SettForm.ShowDialog(this);
            Application.Restart();
        }

        
        void gbxStartStop_Click(object sender, EventArgs e)
        {
            int timerTime = Convert.ToInt32((Controls["gbx_groupbox"].Controls["gbxNumeric"] as NumericUpDown).Value);
            timerTime = timerTime * 60000;
            //1 минута = 60 секунд = 60000 мс.
            timer1.Interval = timerTime;

            

            if (timer1.Enabled) 
            { 
                timer1.Stop();
                toolStripStatusLabel1.Text = "Программа неактивна";
                (Controls["gbx_groupbox"].Controls["gbxStartStop"] as Button).Text = "Старт";
                //отключаемся от джаббера
                jabber_connection.Close();
            }
            else
            {
                timer1.Start();
                (Controls["gbx_groupbox"].Controls["gbxStartStop"] as Button).Text="Стоп";
                toolStripStatusLabel1.Text = "Программа работает";
                //коннектимся к джабберу
                jabber_connection.Connect();                
            }
            
            

            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            
            jabber_connection.Close();
        }

          


       
       

      
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                //Сворачиваем в трей
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500, "Программа работает", "Работает", ToolTipIcon.Info);
                this.Hide();
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Переводит заданное время в формат UnixTime, чтобы отослать в mqtt сообщении.
        /// </summary>
        /// <param name="dt">DateTime Объект</param>
        private string ConvertToUnixTime(DateTime dt)
        {
            string text = (dt - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds.ToString();
            return text;
        }


        

        
            


        private void timer1_Tick(object sender, EventArgs e)
        {
            
            DateTime CurrentTime = DateTime.Now;
            //отнимем число минут из NumericUpDown от текущего, тем самым получим время прошлой проверки
            int timerTime = Convert.ToInt32((Controls["gbx_groupbox"].Controls["gbxNumeric"] as NumericUpDown).Value);
            CurrentTime = CurrentTime - new TimeSpan(0, timerTime, 0);

            
            
            

            //Начинаем отслеживание
            foreach (AltaDatabase gtdbase in baseList)
            {


                if (gtdbase.check)
                {
                    if (gtdbase.TestConnect())
                    {
                        List<string> GtdList = new List<string>();
                        //получаем список поданных, но не выпущенных гтд по фирме
                        GtdList = gtdbase.GetGtdList();
                        if (GtdList.Count == 1)
                        {
                            string s = GtdList.FirstOrDefault();
                            if (s == "")
                            {
                                gtdbase.infoLabel.Text = "Похоже всё выпущено :)";
                                break;
                            } // Если нет ни одной декларации, то всё равно запрос выдаёт пустую строку и количество элементов GtdList будет считаться равным 1.
                        }
                        
                        gtdbase.infoLabel.Text = "Отслеживается деклараций: " + GtdList.Count.ToString();
                        foreach (string s in GtdList)
                        {
                             
                            //Получим инфу о декларации
                            string[] Info = gtdbase.GetGtdInfo(s);
                            //получаем список эд-2 сообщений
                            List<EdMsg> EdMessages = new List<EdMsg>();
                            EdMessages = gtdbase.GetEdMessages(s, addtosql);
                            //Теперь проверим время этих сообщений и сравним с временем последней проверки

                            foreach (EdMsg msg in EdMessages)
                            {
                                if (msg.MessageType != "")
                                {
                                    DateTime MessageTime = Convert.ToDateTime(msg.InOutDateTime);
                                    if (MessageTime >= CurrentTime)
                                    {
                                        //Свежее сообщение
                                        string time = MessageTime.Day.ToString() + "." + MessageTime.Month.ToString() + "." + MessageTime.Year.ToString() + " " + MessageTime.Hour.ToString() + ":" + MessageTime.Minute.ToString();
                                        ListViewItem lvi = new ListViewItem(time);
                                        lvi.SubItems.Add(gtdbase.firmName);
                                        lvi.SubItems.Add(msg.MessageType);
                                        lvi.SubItems.Add(msg.Result);
                                        string gtdNumber = "";
                                        if (Info[4] == "") { gtdNumber = Info[5]; lvi.SubItems.Add(Info[5]); } else { gtdNumber = Info[4]; lvi.SubItems.Add(Info[4]); } //Если есть таможенный номер, показываем его, иначе внутренний альтовский
                                        (Controls["gbx_groupbox"].Controls["gbxlistView"] as ListView).Items.Add(lvi);


                                        string message = message = "Фирма: " + gtdbase.firmName + "\r\nТип: " + msg.MessageType + " - " + MessageDescriptions[msg.MessageType] +
                                                                   "\r\n Время: " + MessageTime.Hour.ToString() + ":" + MessageTime.Minute.ToString()+ ":" + MessageTime.Second.ToString() +
                                                                   "\r\nРезультат: " + msg.Result + "\r\n( ТД №" + gtdNumber + " )";
                                        jabber_connection.SendMessage(gtdbase.whoToNotify, message);

                                        
                                        // Фирма: Пупкин\r\n
                                        // Тип: CMN.0001 - Описание
                                        // Результат: - 
                                        // (ТД № ______)
                                       

                                       

                                    }
                                }

                            }
                           

                        }

                    }
                    else
                    {
                        gtdbase.infoLabel.Text = "Нет связи с MSSQL сервером. Проверьте настройки или его доступность.";
                        gtdbase.panel.BackColor = Color.LightPink;
                    }
                }
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }  

       

      
    }
}
